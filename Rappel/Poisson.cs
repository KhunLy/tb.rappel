﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rappel
{
    public class Poisson
    {
        public event Action<string> ChangementEtat;

        private int pv = 20;

        public int Pv
        {
            get
            {
                return pv;
            }

            set
            {
                pv = value;
                if(pv <= 0)
                {
                    ChangementEtat("Mort");
                }
            }
        }
    }
}
