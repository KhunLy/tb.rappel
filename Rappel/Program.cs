﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Rappel
{
    //delegate void MyDelegate(); 
    class Plante { }
    class Program
    {
        static void Main(string[] args)
        {
            List<int> l = new List<int> { 41, 42, 12, 17, 1, 99 };

            List<string> l2 = new List<string> { "Khun", "Michael" };
            Filtrer(l2, x => x.Length > 4);

            //Ecole e = new Ecole();
            ////Etudiant s = e.Chercher("Ly");

            //e[""] = new Etudiant { Nom = "Morre", Prenom = "Thierry" };
            //Etudiant s = e["Thierry"];
            //Console.WriteLine(s.Nom + " " + s.Prenom);

            //object a = new Chat { Age = 7 };
            //Animal ec = new Ecureuil { Age = 7 };
            //Console.WriteLine((a as Chat)?.GetAgeHumain() ?? (object)"Pas un chat");


            //Poisson p = new Poisson();
            //p.ChangementEtat += etat =>
            //{
            //    Console.WriteLine($"Le nouvel etat de mon poisson est " + etat);
            //};
            //Console.ReadKey();
            //p.Pv -= 5;
            //Console.ReadKey();
            //p.Pv -= 5;
            //Console.ReadKey();
            //p.Pv -= 5;
            //Console.ReadKey();
            //p.Pv -= 5;
            //Console.ReadKey();

            //GeolocService geoloc = new GeolocService();

            //geoloc.Action += (lat, lon) =>
            //{
            //    Console.WriteLine($"lat: {lat}, lng: {lon}");
            //};

            //geoloc.Action += (lat, lon) =>
            //{
            //    Console.WriteLine($"Coucou");
            //};

            //geoloc.Action(0, 0);

            Console.ReadKey();
            //List<int> l = new List<int>
            //{
            //    1,2,3,42,55,99,0
            //};

            //Filtrer(l, nb => { 
            //    return EstPair(nb) && EstPlusGrandQue5(nb); 
            //});

            //Filtrer(l, x => x > 2 );

            //FiltrerPair(l);

            //Filtrer(l, EstPair);
            //Filtrer(l, EstPlusGrandQue5);


            //List<int> l2 = FiltrerPair(l).ToList();

            //foreach (int nb in l2)
            //{
            //    Console.WriteLine(nb);
            //}

        }

        static bool EstPair(int nb)
        {
            return nb % 2 == 0;
        }

        static bool EstPlusGrandQue5(int nb)
        {
            return nb > 5;
        }

        static /*IEnumerable<int>*/ void FiltrerPair(List<int> listeDepart)
        {
            foreach(int i in listeDepart)
            {
                if (EstPair(i))
                {
                    Console.WriteLine(i);
                    /*yield return i;*/
                }
            }
        }
        //delegate bool Mydelegate(int nb);

        static void Filtrer<T>(List<T> liste, Func<T, bool> del)
        {
            foreach (T i in liste)
            {
                if (del(i))
                {
                    Console.WriteLine(i);
                    /*yield return i;*/
                }
            }
        }
    }
}
