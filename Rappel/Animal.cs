﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rappel
{
    abstract class Animal
    {
        public int Age { get; set; }

        public abstract int Coefficient { get; }

        public int GetAgeHumain()
        {
            return Age * Coefficient;
        }
    }
}
