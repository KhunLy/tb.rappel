﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Rappel
{
    public class GeolocService
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        private Random _rand = new Random();

        public event Action<double, double> Action;

        public GeolocService()
        {
            Task.Run(() =>
            {
                while(true)
                {
                    System.Threading.Thread.Sleep(2000);
                    Latitude = _rand.Next(-90, 90);
                    Longitude = _rand.Next(-90, 90);
                    Action(Latitude, Longitude);
                }
            });
        }
    }
}
