﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rappel
{
    class Ecole
    {
        private List<Etudiant> _etudiant = new List<Etudiant> { 
            new Etudiant { Nom = "Lootens", Prenom = "Dylan" },
            new Etudiant { Nom = "Ching", Prenom = "Bike" },
            new Etudiant { Nom = "Person", Prenom = "Mike" },
            new Etudiant { Nom = "Ly", Prenom = "Khun" },
        };

        public Etudiant this[string nomOuPrenom]
        {
            get
            {
                return Chercher(nomOuPrenom);
            }

            set
            {
                Ajouter(value);
            }
        }

        public Etudiant Chercher(string nomOuPrenom)
        {
            foreach (Etudiant e in _etudiant)
            {
                if(e.Nom == nomOuPrenom || e.Prenom == nomOuPrenom)
                {
                    return e;
                }
            }
            return null;
        }

        public void Ajouter(Etudiant e)
        {
            _etudiant.Add(e);
        }

        
    }

    class Etudiant
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
    }
}
